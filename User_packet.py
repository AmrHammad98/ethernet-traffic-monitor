# a class that defines the packet created by the user
class Userpack(object):
    # constructor
    def __init__(self):
        # attributes
        self.count = None                       # how many times the user wants to send the same packet
        self.id = None                          # id to be used for removal

        # Ethernet segment
        self.usr_src_mac = None                 # user input for source mac
        self.usr_dst_mac = None                 # user input for destination mac
        self.ether_seg = None                   # scapy Ether() to be created
        self.ether_flag = False                 # flag that ether part is available

        # IP segment
        self.usr_src_ip = None                  # user input for source ip
        self.usr_dst_ip = None                  # user input for destination ip
        self.ip_seg = None                      # scapy IP() to be created
        self.ip_flag = False                    # flag that ip part is available

        # TCP/UDP segment
        self.usr_src_port = None                # user input for source port
        self.usr_dst_port = None                # user input for destination port

        # TCP
        self.tcp_seg = None                     # scapy TCP() to be created
        self.tcp_flag = False                   # flag that tcp part is available

        # UDP
        self.udp_seg = None                     # scapy UDP() to be created
        self.udp_flag = False                   # flag that udp is available
