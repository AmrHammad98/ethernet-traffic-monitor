from PyQt5 import QtCore, uic
from PyQt5.QtWidgets import QCompleter, QMessageBox, QFileDialog
from Table_Model import TableModel
from Sniffer import Sniffer
from Tree import Tree
from scapy import utils
from scapy.arch.windows import get_windows_if_list
from CreateWnd import packWindow
from Read_Write import fileHandler

base, form = uic.loadUiType("MainWnd.ui")

# A class that defines the main window of GUI
class MainWindow(base, form):
    # constructor
    def __init__(self, parent=None):
        super(base, self).__init__(parent)
        self.setupUi(self)

        # table data to be inserted and headers
        self.t_headers = ["id", "Time", "Source IP", "Destination IP", "Protocol", "Info"]
        self.t_data = []

        # Connecting Check-boxes with event handlers
        self.idchk.stateChanged.connect(self.check_box_id)
        self.timechk.stateChanged.connect(self.check_box_ti)
        self.srcchk.stateChanged.connect(self.check_box_src)
        self.dstchk.stateChanged.connect(self.check_box_dst)
        self.prchk.stateChanged.connect(self.check_box_pr)

        # Connecting buttons with event handlers
        self.StartBtn.clicked.connect(self.start_on_click)
        self.StopBtn.clicked.connect(self.stop_on_click)
        self.srch_Btn.clicked.connect(self.set_search)
        self.fltr_Btn.clicked.connect(self.set_filter)
        self.clrBtn.clicked.connect(self.clr_on_click)
        self.create_btn.clicked.connect(self.create_on_click)

        # Initializing models and views and linking them
        self._model = TableModel(self.t_data, self.t_headers)
        self._proxyModel = QtCore.QSortFilterProxyModel()
        self._proxyModel.setSourceModel(self._model)

        # enabling dynamic sorting re-sorts and re-filters data whenever the original model changes
        self._proxyModel.setDynamicSortFilter(True)

        self.tableView.setModel(self._proxyModel)

        # connect edit finished to handlers
        self.srchtxt.editingFinished.connect(self.srch_on_efin)
        self.filtertxt.editingFinished.connect(self.fltr_on_efin)

        # enabling sorting in table view
        self.tableView.setSortingEnabled(True)

        self.count = 0                             # records count
        self.record = None                         # record to be appended in t_data
        self.sniffer = Sniffer(self.record, self._model, self.t_data, self.count)              # a thread where the sniffer operates

        # connect tableview selection signal with handler
        self.tableView.selectionModel().selectionChanged.connect(self.item_selected)

        # tree widget
        self.tree = Tree(self.tree1)

        # set hexa text edit to read only
        self.hexa.setReadOnly(True)

        # suggestions for filtering
        filter_sugg = ["ether dst ff:ff:ff:ff:ff:ff", "ether src ff:ff:ff:ff:ff:ff", "dst port 443", "src port 443", "port 23", "tcp", "tcp and src port 443", "tcp and dst port 443",
                       "tcp and port 7", "udp", "udp and src port 443", "udp and dst port 443", "udp and port 5", "icmp", "ip", "ip and src 127.0.0.1", "ip and dst 127.0.0.1"]
        filter_completer = QCompleter(filter_sugg)                                      # completer for filter line edit
        filter_completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)

        # set completer for filter line edit
        self.filtertxt.setCompleter(filter_completer)

        # suggestions for searching
        self.ip_sugg = ["127.0.0.1", "None"]
        self.proto_sugg = ["TCP", "UDP", "ICMP", "Ethernet"]

        # search completer
        self.search_completer = None

        # sniffer's interface
        self.interface = None

        # connect combobox to handler
        self.comboBox.currentTextChanged.connect(self.combo_on_change)

        self.packwnd = None                                                 # pop up window when create and send button is pressed

        self.get_add_interfaces()

        # tree selection
        self.tree1.itemClicked.connect(self.tree_item_selected)
        self.hexdump = None
        self.selected_flag = False                                          # flags a selection of an item

        self.StopBtn.setEnabled(False)

        # file handler
        self.handler = fileHandler(None, self.t_data, self._model)

        # connecting actions to handlers (menubar and toolbar)
        self.actionExport.triggered.connect(self.actionExport_on_triggered)
        self.actionImport.triggered.connect(self.actionImport_on_triggered)
        self.actionStart.triggered.connect(self.actionStart_on_triggered)
        self.actionStop.triggered.connect(self.actionStop_on_triggered)
        self.actionClear.triggered.connect(self.actionClear_on_triggered)

        # import flag
        self.import_flag = False

    ##################################################
    # CHECK BOXES HANDLERS
    ##################################################

    def check_box_id(self):
        if self.idchk.isChecked():
            self.timechk.setEnabled(False)
            self.srcchk.setEnabled(False)
            self.dstchk.setEnabled(False)
            self.prchk.setEnabled(False)
            self._proxyModel.setFilterKeyColumn(0)
        else:
            self.timechk.setEnabled(True)
            self.srcchk.setEnabled(True)
            self.dstchk.setEnabled(True)
            self.prchk.setEnabled(True)

    def check_box_ti(self):
        if self.timechk.isChecked():
            self.idchk.setEnabled(False)
            self.srcchk.setEnabled(False)
            self.dstchk.setEnabled(False)
            self.prchk.setEnabled(False)
            self._proxyModel.setFilterKeyColumn(1)
        else:
            self.idchk.setEnabled(True)
            self.srcchk.setEnabled(True)
            self.dstchk.setEnabled(True)
            self.prchk.setEnabled(True)

    def check_box_src(self):
        # set tool tip
        self.srchtxt.setToolTip("Source IP format xx.xx.xx.xx, ex: 127.0.0.1 ...etc")

        # set completer
        self.search_completer = QCompleter(self.ip_sugg)
        self.srchtxt.setCompleter(self.search_completer)
        self.search_completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)

        if self.srcchk.isChecked():
            self.idchk.setEnabled(False)
            self.timechk.setEnabled(False)
            self.dstchk.setEnabled(False)
            self.prchk.setEnabled(False)
            self._proxyModel.setFilterKeyColumn(2)
        else:
            self.idchk.setEnabled(True)
            self.timechk.setEnabled(True)
            self.dstchk.setEnabled(True)
            self.prchk.setEnabled(True)
            # set tool tip
            self.srchtxt.setToolTip("Search captured packets.")

    def check_box_dst(self):
        # set tool tip
        self.srchtxt.setToolTip("Destination IP format xx.xx.xx.xx, ex: 127.0.0.1 ...etc")

        # set completer
        self.search_completer = QCompleter(self.ip_sugg)
        self.srchtxt.setCompleter(self.search_completer)
        self.search_completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)

        if self.dstchk.isChecked():
            self.idchk.setEnabled(False)
            self.srcchk.setEnabled(False)
            self.timechk.setEnabled(False)
            self.prchk.setEnabled(False)
            self._proxyModel.setFilterKeyColumn(3)
        else:
            self.idchk.setEnabled(True)
            self.srcchk.setEnabled(True)
            self.timechk.setEnabled(True)
            self.prchk.setEnabled(True)
            # set tool tip
            self.srchtxt.setToolTip("Search captured packets.")

    def check_box_pr(self):
        # set tool tip
        self.srchtxt.setToolTip("Protocol ex: tcp, udp ...etc")

        # set completer
        self.search_completer = QCompleter(self.proto_sugg)
        self.srchtxt.setCompleter(self.search_completer)
        self.search_completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)

        if self.prchk.isChecked():
            self.idchk.setEnabled(False)
            self.srcchk.setEnabled(False)
            self.dstchk.setEnabled(False)
            self.timechk.setEnabled(False)
            self._proxyModel.setFilterKeyColumn(4)
        else:
            self.idchk.setEnabled(True)
            self.srcchk.setEnabled(True)
            self.dstchk.setEnabled(True)
            self.timechk.setEnabled(True)
            # set tool tip
            self.srchtxt.setToolTip("Search captured packets.")

    ##################################################
    # BUTTONS HANDLERS
    ##################################################

    # start button handler
    def start_on_click(self):
        # in case imported before sniffing
        if self.import_flag:
            self.import_flag = False
            reply = QMessageBox.question(self, "Remove", "Starting sniffing will remove all imported packets\nAre you sure you want to continue?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:

                # remove all old data
                self.handler.remove_all()

                # start sniffer
                self.sniffer.start_sniff(self.filtertxt.text(), self.interface)

                # disable start button
                self.StartBtn.setEnabled(False)

                # Enable stop button
                self.StopBtn.setEnabled(True)

                # disable interface combobox
                self.inter_grpbx.setEnabled(False)
            else:
                pass
        else:
            # start sniffer
            self.sniffer.start_sniff(self.filtertxt.text(), self.interface)

            # disable start button
            self.StartBtn.setEnabled(False)

            # Enable stop button
            self.StopBtn.setEnabled(True)

            # disable interface combobox
            self.inter_grpbx.setEnabled(False)
            
    # stop button handler
    def stop_on_click(self):
        # stop sniffer
        self.sniffer.stop_sniff()

        # disable stop button
        self.StopBtn.setEnabled(False)

        # Enable start button
        self.StartBtn.setEnabled(True)

        # enable interface combobox
        self.inter_grpbx.setEnabled(True)

        # save dialog
        reply = QMessageBox.question(self, "Save", "Do you wish to save captured packets?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            # self.handler.write_pcap()
            fn = QFileDialog.getSaveFileName(self, caption='Save File', filter="pcap (*.*)")
            if fn[0] is not "":
                self.handler.filename = fn[0]
                self.handler.write_pcap()
        else:
            pass

    # create btn handler send and create window
    def create_on_click(self):
        self.packwnd = packWindow()
        self.packwnd.show()

    # a method for searching in records
    def set_search(self):
        if self.sniffer.sniff_flag == True:
            self.sniffer.stop_sniff()
            self._proxyModel.setFilterRegExp(self.srchtxt.text())
            self.sniffer.start_sniff(self.filtertxt.text(), self.interface)
        else:
            self._proxyModel.setFilterRegExp(self.srchtxt.text())

    # a method to set sniffer's filter
    def set_filter(self):
        if self.sniffer.sniff_flag == True:
            self.sniffer.stop_sniff()

            self.sniffer.start_sniff(self.filtertxt.text(), self.interface)

    # clear button handler
    def clr_on_click(self):
        if self.import_flag:
            # remove all old data
            self.handler.remove_all()

            self.import_flag = False
        # hide all entries
        self.hide_all()

    ##################################################
    # EDITING FINISHED HANDLERS
    ##################################################
    def srch_on_efin(self):
        if self.sniffer.sniff_flag == True:
            self.sniffer.stop_sniff()
            self._proxyModel.setFilterRegExp(self.srchtxt.text())
            self.sniffer.start_sniff(self.filtertxt.text(), self.interface)
        else:
            self._proxyModel.setFilterRegExp(self.srchtxt.text())

    def fltr_on_efin(self):
        if self.sniffer.sniff_flag == True:
            self.sniffer.stop_sniff()

            if self.filtertxt.text() == "":
                self.show_all()
            else:
                self.hide_all()

            self.sniffer.start_sniff(self.filtertxt.text(), self.interface)

    ##################################################
    # SELECTION HANDLER
    ##################################################

    # when a record in a table is selected
    def item_selected(self):
        # raise flag
        self.selected_flag = True

        indices = self.tableView.selectionModel().selectedIndexes()          # selected indices

        # check if empty
        if not indices:
            pass
        else:
            v_index = indices[0]                                                 # index in view

            # select row
            self.tableView.selectRow(v_index.row())

            # know the sort order to get the correct index in model
            # Ascending order
            if self.tableView.horizontalHeader().sortIndicatorOrder() == 0:
                index = self._model.index(v_index.row(), 0).data()
                self.tree.updateChild(self.t_data[index])

            # Descending order
            elif self.tableView.horizontalHeader().sortIndicatorOrder() == 1:
                index = self._model.index((len(self.t_data)-1) - v_index.row(), 0).data()
                self.tree.updateChild(self.t_data[index])

            # hexdump
            self.hexdump = utils.hexdump(self.t_data[index].pack, True)
            self.hexa.setPlainText(self.hexdump)

    # when a tree item is selected
    def tree_item_selected(self):
        if self.selected_flag:

            # split hexdumpnto lines to make it easier in classification
            split_ln = self.hexdump.splitlines()
            # print(split_ln)

            # lines
            if len(split_ln) >= 1 : l1 = split_ln[0]                        # Ethernet
            if len(split_ln) >= 2 : l2 = split_ln[1]                        # IP
            if len(split_ln) >= 3 : l3 = split_ln[2]                        # IP/ TCP/ UDP
            if len(split_ln) >= 4 : l4 = split_ln[3]                        # TCP

            # Destination mac
            if self.tree1.currentItem().type() == 1:
                self.hexa.setPlainText(l1[6:23])
            # Source mac
            elif self.tree1.currentItem().type() == 2:
                self.hexa.setPlainText(l1[24:41])
            # type
            elif self.tree1.currentItem().type() == 3:
                self.hexa.setPlainText(l1[42:47])
            # ethernet payload
            elif self.tree1.currentItem().type() == 29:
                str = None
                for i in split_ln:
                    if i == split_ln[0]:
                        str = i[47:]
                    else:
                        str = str + "\n" + i
                self.hexa.setPlainText(str)
            # IP
            # ver
            elif self.tree1.currentItem().type() == 4:
                self.hexa.setPlainText(l1[48:50])
            # ihl
            elif self.tree1.currentItem().type() == 5:
                self.hexa.setPlainText(l1[48:50])
            # tos
            elif self.tree1.currentItem().type() == 6:
                self.hexa.setPlainText(l1[51:53])
            # total length
            elif self.tree1.currentItem().type() == 7:
                self.hexa.setPlainText(l2[6:11])
            # ID
            elif self.tree1.currentItem().type() == 8:
                self.hexa.setPlainText(l2[12:18])
            # flags
            elif self.tree1.currentItem().type() == 9:
                self.hexa.setPlainText(l2[18:23])
            # ttl
            elif self.tree1.currentItem().type() == 11:
                self.hexa.setPlainText(l2[24:27])
            # protocol
            elif self.tree1.currentItem().type() == 12:
                self.hexa.setPlainText(l2[27:30])
            # checksum
            elif self.tree1.currentItem().type() == 13:
                self.hexa.setPlainText(l2[30:36])
            # src ip
            elif self.tree1.currentItem().type() == 14:
                self.hexa.setPlainText(l2[36:48])
            # dst ip
            elif self.tree1.currentItem().type() == 15:
                self.hexa.setPlainText(l2[48:54] + l3[6:12])
            # ip payload
            elif self.tree1.currentItem().type() == 30:
                str = None
                for i in split_ln:
                    if i == split_ln[2]:
                        str = i[12:]
                    elif i == split_ln[0]:
                        pass
                    elif i == split_ln[1]:
                        pass
                    else:
                        str = str + "\n" + i
                self.hexa.setPlainText(str)
            # UDP
            # src port
            elif self.tree1.currentItem().type() == 16:
                self.hexa.setPlainText(l3[12:18])
            # dst port
            elif self.tree1.currentItem().type() == 17:
                self.hexa.setPlainText(l3[18:24])
            # length
            elif self.tree1.currentItem().type() == 18:
                self.hexa.setPlainText(l3[24:30])
            # checksum
            elif self.tree1.currentItem().type() == 19:
                self.hexa.setPlainText(l3[30:36])
            # payload
            elif self.tree1.currentItem().type() == 32:
                str = None
                for i in split_ln:
                    if i == split_ln[2]:
                        str = i[36:]
                    elif i == split_ln[0]:
                        pass
                    elif i == split_ln[1]:
                        pass
                    else:
                        str = str + "\n" + i
                self.hexa.setPlainText(str)
            # TCP
            # src port
            elif self.tree1.currentItem().type() == 20:
                self.hexa.setPlainText(l3[12:18])
            # dst port
            elif self.tree1.currentItem().type() == 21:
                self.hexa.setPlainText(l3[18:24])
            # seq
            elif self.tree1.currentItem().type() == 22:
                self.hexa.setPlainText(l3[24:36])
            # ackno
            elif self.tree1.currentItem().type() == 23:
                self.hexa.setPlainText(l3[36:48])
            # flags
            elif self.tree1.currentItem().type() == 24:
                self.hexa.setPlainText(l3[48:54])
            # window
            elif self.tree1.currentItem().type() == 25:
                self.hexa.setPlainText(l4[6:12])
            # checksum
            elif self.tree1.currentItem().type() == 26:
                self.hexa.setPlainText(l4[12:18])
            # urg pointer
            elif self.tree1.currentItem().type() == 27:
                self.hexa.setPlainText(l4[18:24])
            # payload
            elif self.tree1.currentItem().type() == 31:
                str = None
                for i in split_ln:
                    if i == split_ln[3]:
                        str = i[24:]
                    elif i == split_ln[0] or i == split_ln[1] or i == split_ln[2]:
                        pass
                    else:
                        str = str + "\n" + i
                self.hexa.setPlainText(str)
                # self.hexa.setPlainText(l3[12:54] + l4[6:24])
            else:
                self.hexa.setPlainText(self.hexdump)

    ##################################################
    # COMBOBOX Methods
    ##################################################

    # Handler to combobox selection changed
    def combo_on_change(self):
        self.interface = self.comboBox.currentText()
        self.hide_all()

    # a method to get available interfaces to combobox and add them
    def get_add_interfaces(self):
        # store available interfaces with information in a list
        av_ifaces = get_windows_if_list()

        # get interfaces names and add them to combobox
        for x in av_ifaces:
            self.comboBox.addItem(x.get("name"))

    ##################################################
    # HIDE & SHOW
    ##################################################
    def hide_all(self):
        for i in range(self.sniffer.count):
            self.tableView.hideRow(i)

    def show_all(self):
        for i in range(self.sniffer.count):
            self.tableView.showRow(i)

    ##################################################
    # ACTION HANDLERS
    ##################################################

    # a method to handle export action
    def actionExport_on_triggered(self):
        # stop sniffer
        self.sniffer.stop_sniff()

        # create a file dialog
        fn = QFileDialog.getSaveFileName(self, caption='Save File', filter="pcap (*.*)")
        if fn[0] is not "":
            self.handler.filename = fn[0]
            self.handler.write_pcap()

        # resume sniffer
        if self.StopBtn.isEnabled():
            self.sniffer.start_sniff(self.filtertxt.text(), self.interface)

    # a method to handle import action
    def actionImport_on_triggered(self):
        self.import_flag = True
        fn = QFileDialog.getOpenFileName(self, caption="Open File", filter="pcap (*.*)")
        if fn[0] is not "":
            self.handler.filename = fn[0]
            self.handler.read_pcap()

            # classify packets
            self.handler.classify(self.handler.r_data)

    # a method to handle start action
    def actionStart_on_triggered(self):
        # in case imported before sniffing
        if self.import_flag:
            self.import_flag = False
            reply = QMessageBox.question(self, "Remove", "Starting sniffing will remove all imported packets\nAre you sure you want to continue?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:

                # remove all old data
                self.handler.remove_all()

                # start sniffer
                self.sniffer.start_sniff(self.filtertxt.text(), self.interface)

                # disable start button
                self.StartBtn.setEnabled(False)

                # Enable stop button
                self.StopBtn.setEnabled(True)

                # disable interface combobox
                self.inter_grpbx.setEnabled(False)
            else:
                pass
        else:
            # start sniffer
            self.sniffer.start_sniff(self.filtertxt.text(), self.interface)

            # disable start button
            self.StartBtn.setEnabled(False)

            # Enable stop button
            self.StopBtn.setEnabled(True)

            # disable interface combobox
            self.inter_grpbx.setEnabled(False)

    # a method to handle stop action
    def actionStop_on_triggered(self):
        # stop sniffer
        self.sniffer.stop_sniff()

        # disable stop button
        self.StopBtn.setEnabled(False)

        # Enable start button
        self.StartBtn.setEnabled(True)

        # enable interface combobox
        self.inter_grpbx.setEnabled(True)

        # save dialog
        reply = QMessageBox.question(self, "Save", "Do you wish to save captured packets?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            # self.handler.write_pcap()
            fn = QFileDialog.getSaveFileName(self, caption='Save File', filter="pcap (*.*)")
            if fn[0] is not "":
                self.handler.filename = fn[0]
                self.handler.write_pcap()
        else:
            pass

    # a method to handle clear action
    def actionClear_on_triggered(self):
        if self.import_flag:
            # remove all old data
            self.handler.remove_all()

            self.import_flag = False
        # hide all entries
        self.hide_all()
