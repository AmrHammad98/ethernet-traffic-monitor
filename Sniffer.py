from PyQt5 import QtCore
from scapy.all import *
from Record_class import Record
from Layers import *
from time import *

class Sniffer(Thread):
    # constructor
    def __init__(self, record, model, data, count):
        self.record = record                            # packet record
        self.model = model                              # table model
        self.count = count                              # records count
        self.data = data                                # model's data
        self.sniff_thread = None                        # sniffing thread
        self.start_time = None                          # sniffing starting time
        self.current_time = None                        # current time
        self.elapsed_time = None                        # time elapsed since sniffing has started
        self.sniff_flag = False                         # flag that sniffer has started

    # a method to start the async sniffer
    def start_sniff(self, filter, interface):
        if not self.sniff_flag:
            if len(self.data) == 0:
                self.count = 0
            self.start_time = time()
            self.sniff_thread = AsyncSniffer(filter=filter, prn=self.classify, store=False, iface=interface)
            self.sniff_thread.start()
            # Raise sniff flag
            self.sniff_flag = True

    # a method to stop the async sniffer
    def stop_sniff(self):
        if self.sniff_flag:
            self.sniff_thread.stop()
            # Down sniff flag
            self.sniff_flag = False

    # update model's underlaying data in a way to be updated all views
    def update_model(self):
            self.model.beginInsertRows(QtCore.QModelIndex(), self.count, self.count)
            self.data.append(self.record)
            self.count += 1
            self.model.endInsertRows()

    # a method that converts protocol numbers to their corresponding name
    def get_proto(self, protoNo):
        if (protoNo == 6):
            return "TCP"
        elif (protoNo == 17):
            return "UDP"
        elif (protoNo == 1):
            return "ICMP"

    # a method that classifies network layers invoked by scapy's sniff function
    def classify(self, packets):
        for packet in packets:
            # get current time
            self.current_time = time()

            # calculate elapsed time since sniffing has started
            self.elapsed_time = self.current_time - self.start_time

            # create a record object
            self.record = Record(self.count, self.elapsed_time)

            # store packet in record
            self.record.pack = packet

            # Ethernet (Create ethernet object that is stored in a record object)
            if (packet.haslayer('Ethernet')):
                eth_p = packet.getlayer('Ethernet')
                self.record.ethernet = Eth_part(eth_p.src, eth_p.dst, eth_p.type)

                self.record.information = f"Source mac: {eth_p.src} Destination mac: {eth_p.dst} Type: {eth_p.type}"

            # IP  (Create ip object that is stored in a record object)
            if (packet.haslayer('IP')):
                ip_p = packet.getlayer('IP')
                self.record.ip = IP_part(ip_p.version, ip_p.ihl, ip_p.tos, ip_p.len, ip_p.id, ip_p.flags, ip_p.frag, ip_p.ttl, self.get_proto(ip_p.proto), ip_p.chksum, ip_p.src, ip_p.dst)

            # ICMP  (Create ICMP object that is stored in a record object)
            if (packet.haslayer('ICMP')):
                icmp_p = packet.getlayer('ICMP')
                self.record.icmp = ICMP_part(icmp_p.type, icmp_p.code, icmp_p.chksum, icmp_p.id, icmp_p.seq)

                # setting info part
                self.record.information = f"Type: {icmp_p.type}, Code: {icmp_p.code}, Checksum:  {icmp_p.chksum}, Seq: {icmp_p.seq}"
            # TCP
            if (packet.haslayer('TCP')):
                tcp_p = packet.getlayer('TCP')
                self.record.tcp = TCP_part(tcp_p.sport, tcp_p.dport, tcp_p.seq, tcp_p.ack, tcp_p.dataofs, tcp_p.reserved, tcp_p.flags, tcp_p.window, tcp_p.chksum, tcp_p.urgptr, tcp_p.options)

                # setting info part
                self.record.information = f"Source Port: {tcp_p.sport} Destination Port: {tcp_p.dport}"
            # UDP
            if (packet.haslayer('UDP')):
                udp_p = packet.getlayer('UDP')
                self.record.udp = UDP_part(udp_p.sport, udp_p.dport, udp_p.len, udp_p.chksum)

                # setting info part
                self.record.information = f"Source Port: {udp_p.sport} Destination Port: {udp_p.dport} Length: {udp_p.len}"
            # Payload
            if (packet.haslayer('Raw')):
                self.record.load = Payload_part(packet.getlayer(Raw).load)

            # update model
            self.update_model()