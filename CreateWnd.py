from PyQt5 import uic
from scapy.layers.l2 import Ether
from scapy import sendrecv
from scapy.layers.inet import IP, TCP, UDP
from scapy.arch.windows import get_windows_if_list
from User_packet import Userpack
from PyQt5.QtWidgets import QFileDialog, QListWidgetItem
import pickle

base, form = uic.loadUiType("Createwnd.ui")

# A class that defines the main window of GUI
class packWindow(base, form):
    # constructor
    def __init__(self, parent=None):
        super(base, self).__init__(parent)
        self.setupUi(self)

        # interface
        self.interface = None                       # current network interface
        self.id_count = 0

        # flags
        self.tcp_box_flag = False                   # flag that tcp check box is checked
        self.udp_box_flag = False                   # flag that udp check box is checked

        self.packs = []                             # a list that contains packets to be sent
        self.pk = None                              # a packet to be added in the list

        # connect buttons to handlers
        self.sendBtn.clicked.connect(self.send_on_click)
        self.addBtn.clicked.connect(self.add_on_click)

        # connect check boxes to handlers
        self.tcp_chk.stateChanged.connect(self.tcp_on_chk)
        self.udp_chk.stateChanged.connect(self.udp_on_chk)

        # uncheck all group boxes
        self.eth_box.setChecked(False)
        self.ip_box.setChecked(False)
        self.tu_box.setChecked(False)

        # connect combobox to handler
        self.comboBox.currentTextChanged.connect(self.combo_on_change)

        # get and add available interfaces to combobox
        self.get_add_interfaces()

        # connecting actions to handlers
        self.actionAdd.triggered.connect(self.actionAdd_on_triggered)
        self.actionRemove.triggered.connect(self.actionRemove_on_triggered)
        self.actionSend.triggered.connect(self.actionSend_on_triggered)
        self.actionSave.triggered.connect(self.actionSave_on_triggered)
        self.actionLoad.triggered.connect(self.actionLoad_on_triggered)
        self.actionClear.triggered.connect(self.actionClear_on_triggered)

    ##################################################
    # METHODS
    ##################################################

    # a method to convert string to int (true = sport , false = dport)
    def s2i(self, str, state):
        if state:
            if str == "":
                self.pk.usr_src_port = None
            else:
                self.pk.usr_src_port = int(str.strip())
        else:
            if str == "":
                self.pk.usr_dst_port = None
            else:
                self.pk.usr_dst_port = int(str.strip())

    # a method to fill attributes
    def fill_attr(self):
        # create a user packet
        if self.pk is None:
            self.pk = Userpack()

        # set same packet count
        self.pk.count = self.spinBox.value()

        # ethernet field
        if self.eth_box.isChecked():
            if self.src_mac_txt.text() != "":
                self.pk.usr_src_mac = self.src_mac_txt.text()
                self.pk.ether_flag = True
            if self.dst_mac_txt.text() != "":
                self.pk.usr_dst_mac = self.dst_mac_txt.text()
                self.pk.ether_flag = True

        # ip field
        if self.ip_box.isChecked():
            if self.src_ip_txt.text() != "":
                self.pk.usr_src_ip = self.src_ip_txt.text()
                self.pk.ip_flag = True
            if self.dst_ip_txt.text() != "":
                self.pk.usr_dst_ip = self.dst_ip_txt.text()
                self.pk.ip_flag = True

        # tcp/udp field
        if self.tu_box.isChecked():
            if self.tcp_box_flag:
                self.pk.tcp_flag = True
            elif self.udp_box_flag:
                self.pk.udp_flag = True

            # fill usr_src_port
            self.s2i(self.src_prt_txt.text(), True)

            # fill dst_src_port
            self.s2i(self.dst_prt_txt.text(), False)

        # set id for packet
        self.pk.id = self.id_count
        self.id_count += 1

        # add packet to packs
        for i in range(self.pk.count):
            self.packs.append(self.pk)

    # a method to set all attributes to None
    def reset(self):
        # Reset pk
        self.pk = None

        # uncheck all group boxes
        self.eth_box.setChecked(False)
        self.ip_box.setChecked(False)
        self.tu_box.setChecked(False)

    # a method to send
    def send_packet(self, p):
        # ethernet
        if p.ether_flag and not p.ip_flag and not p.tcp_flag and not p.udp_flag:
            sendrecv.sendp(Ether(src=p.usr_src_mac, dst=p.usr_dst_mac)/self.payload_txt.text(), iface=self.interface)
        # ethernet and ip
        elif p.ether_flag and p.ip_flag and not p.tcp_flag and not p.udp_flag:
            sendrecv.sendp(Ether(src=p.usr_src_mac, dst=p.usr_dst_mac)/IP(src=p.usr_src_ip, dst=p.usr_dst_ip)/self.payload_txt.text(), iface=self.interface)
        # ethernet ip and tcp
        elif p.ether_flag and p.ip_flag and p.tcp_flag and not p.udp_flag:
            sendrecv.sendp(Ether(src=p.usr_src_mac, dst=p.usr_dst_mac)/IP(src=p.usr_src_ip, dst=p.usr_dst_ip)/TCP(sport=p.usr_src_port, dport=p.usr_dst_port)/self.payload_txt.text(), iface=self.interface)
        # ethernet ip and udp
        elif p.ether_flag and p.ip_flag and not p.tcp_flag and p.udp_flag:
            sendrecv.sendp(Ether(src=p.usr_src_mac, dst=p.usr_dst_mac)/IP(src=p.usr_src_ip, dst=p.usr_dst_ip)/UDP(sport=p.usr_src_port, dport=p.usr_dst_port)/self.payload_txt.text(), iface=self.interface)
        # ethernet and tcp
        elif p.ether_flag and not p.ip_flag and p.tcp_flag and not p.udp_flag:
            sendrecv.sendp(Ether(src=p.usr_src_mac, dst=p.usr_dst_mac)/TCP(sport=p.usr_src_port, dport=p.usr_dst_port)/self.payload_txt.text(), iface=self.interface)
        # ethernet and udp
        elif p.ether_flag and not p.ip_flag and not p.tcp_flag and p.udp_flag:
            sendrecv.sendp(Ether(src=p.usr_src_mac, dst=p.usr_dst_mac)/UDP(sport=p.usr_src_port,dport=p.usr_dst_port)/self.payload_txt.text(), iface=self.interface)
        # ip
        elif not p.ether_flag and p.ip_flag and not p.tcp_flag and not p.udp_flag:
            sendrecv.send(IP(src=p.usr_src_ip, dst=p.usr_dst_ip)/self.payload_txt.text(), iface=self.interface)
        # ip and tcp
        elif not p.ether_flag and p.ip_flag and p.tcp_flag and not p.udp_flag:
            sendrecv.send(IP(src=p.usr_src_ip, dst=p.usr_dst_ip)/TCP(sport=p.usr_src_port, dport=p.usr_dst_port)/self.payload_txt.text(), iface=self.interface)
        # ip and udp
        elif not p.ether_flag and p.ip_flag and not p.tcp_flag and p.udp_flag:
            sendrecv.send(IP(src=p.usr_src_ip, dst=p.usr_dst_ip)/UDP(sport=p.usr_src_port, dport=p.usr_dst_port)/self.payload_txt.text(), iface=self.interface)
        # tcp
        elif not p.ether_flag and not p.ip_flag and p.tcp_flag and not p.udp_flag:
            sendrecv.send(TCP(sport=p.usr_src_port, dport=p.usr_dst_port)/self.payload_txt.text(), iface=self.interface)
        # udp
        elif not p.ether_flag and not p.ip_flag and not p.tcp_flag and p.udp_flag:
            sendrecv.send(UDP(sport=p.usr_src_port, dport=p.usr_dst_port)/self.payload_txt.text(), iface=self.interface)

    # a method to add items to list widget
    def add_to_list(self, p):
        type_str = None
        if p.tcp_flag:
            type_str = "TCP"
        elif p.udp_flag:
            type_str = "UDP"
        else:
            type_str = " "
        list_str = f"Src mac: {p.usr_src_mac} Dst mac: {p.usr_dst_mac}\nSrc Ip: {p.usr_src_ip} Dst Ip: {p.usr_dst_ip}\nSrc Port: {p.usr_src_port} Dst Port: {p.usr_dst_port}\nCount: {p.count}\n{type_str}"
        self.packetList.addItem(QListWidgetItem(list_str, type=p.id))

    # a method that gets packet data from queue
    def get_data(self):
        x = 0
        if self.packetList.currentItem() is not None:
            # get packet text
            txt = self.packetList.currentItem().text()

            # split lines
            split_ln = txt.splitlines()

            # ether part
            l1 = split_ln[0]
            x = l1.find("Dst") + 9
            # src mac
            if l1[9] == "N":
                self.src_mac_txt.setText("")
            else:
                self.src_mac_txt.setText(l1[9:26])

            # dst mac
            if l1[x] == "N":
                self.dst_mac_txt.setText("")
            else:
                self.dst_mac_txt.setText(l1[x:])

            # ip part
            l2 = split_ln[1]
            x = l2.find("Dst") + 8

            # src ip
            if l2[8] == "N":
                self.src_ip_txt.setText("")
            else:
                self.src_ip_txt.setText(l2[8:l2.find("Dst")-1])

            # dst ip
            if l2[x] == "N":
                self.dst_ip_txt.setText("")
            else:
                self.dst_ip_txt.setText(l2[x:])

            # tcp/udp part
            l3 = split_ln[2]
            x = l3.find("Dst") + 10

            # src ip
            if l3[10] == "N":
                self.src_prt_txt.setText("")
            else:
                self.src_prt_txt.setText(l3[10:l3.find("Dst")-1])

            # dst ip
            if l3[x] == "N":
                self.dst_prt_txt.setText("")
            else:
                self.dst_prt_txt.setText(l3[x:])

            # spin box
            l4 = split_ln[3]

            self.spinBox.setValue(int(l4[7:].strip()))
            self.old_count = int(l4[7:].strip())                            # old count

            # checkboxes
            l5 = split_ln[4]
            if l5 == "TCP":
                self.tcp_chk.setChecked(True)
            elif l5 == "UDP":
                self.udp_chk.setChecked(True)
            elif l5 == " ":
                self.tcp_chk.setChecked(False)
                self.udp_chk.setChecked(False)

    ##################################################
    # SEND BUTTON HANDLER
    ##################################################

    # Send button handler
    def send_on_click(self):
        # iterate over packs and send
        for i in range(len(self.packs)):
            self.send_packet(self.packs[i])

        self.reset()

    # Add button handler
    def add_on_click(self):
        # fill attributes in user packet object
        self.fill_attr()

        # Reset spinbox
        self.spinBox.setValue(1)

        # add item to packets queue
        self.add_to_list(self.pk)

        self.pk = None

    ##################################################
    # CHECK BOXES HANDLERS
    ##################################################

    # tcp check box handler
    def tcp_on_chk(self):
        if self.tcp_chk.isChecked():
            self.tcp_box_flag = True
            self.udp_chk.setEnabled(False)
        else:
            self.udp_chk.setEnabled(True)
            self.tcp_box_flag = False

    # udp check box handler
    def udp_on_chk(self):
        if self.udp_chk.isChecked():
            self.udp_box_flag = True
            self.tcp_chk.setEnabled(False)
        else:
            self.tcp_chk.setEnabled(True)
            self.udp_box_flag = False

    ##################################################
    # COMBOBOX METHODS
    ##################################################

    # Handler to combobox selection changed
    def combo_on_change(self):
        self.interface = self.comboBox.currentText()

    # a method to get available interfaces to combobox and add them
    def get_add_interfaces(self):
        # store available interfaces with information in a list
        av_ifaces = get_windows_if_list()

        # get interfaces names and add them to combobox
        for x in av_ifaces:
            self.comboBox.addItem(x.get("name"))

    ##################################################
    # ACTION HANDLERS
    ##################################################

    # a method to handle add action
    def actionAdd_on_triggered(self):
        # fill attributes in user packet object
        self.fill_attr()

        # Reset spinbox
        self.spinBox.setValue(1)

        # add item to packets queue
        self.add_to_list(self.pk)

        self.pk = None

    # a method to handle remove action
    def actionRemove_on_triggered(self):
        if self.packetList.currentItem() is not None:
            # remove from queue
            r = self.packetList.currentRow()
            item = self.packetList.item(r)

            if item is None:
                return

            # remove from packs
            for i in self.packs:
                if i.id == item.type():
                    self.packs.remove(i)

            item = self.packetList.takeItem(r)
            del item

    # a method to handle send action
    def actionSend_on_triggered(self):
        # iterate over packs and send
        for i in range(len(self.packs)):
            self.send_packet(self.packs[i])

        self.reset()

    # a method to handle save action
    def actionSave_on_triggered(self):
        # create file dialog to get save file name
        fn = QFileDialog.getSaveFileName(self, caption="Save File")
        if fn:
            # open stream
            out_file = open(fn[0], "wb")
            # dump into file
            pickle.dump(self.packs, out_file)
            # close stream
            out_file.close()

    # a method to handle load action
    def actionLoad_on_triggered(self):
        # create file dialog to get open file name
        fn = QFileDialog.getOpenFileName(self, caption="Load file")
        if fn[0] is not "":
            # open stream
            in_file = open(fn[0], "rb")
            # load from file
            self.packs = pickle.load(in_file)
            # close stream
            in_file.close()

            # clear packet queue
            self.packetList.clear()

            temp = None

            # add loaded packets to queue
            for i in self.packs:
                if i == temp:
                    continue
                else:
                    self.add_to_list(i)
                temp = i

    # a method to handle clear action
    def actionClear_on_triggered(self):
        # clear multiple packets list if not empty
        if not not self.packs:
            self.packs.clear()

        # remove all items in queue
        self.packetList.clear()
