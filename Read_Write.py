from scapy.utils import PcapWriter, PcapReader
from Layers import *
from Record_class import Record
from scapy.packet import Raw
from PyQt5 import QtCore

class fileHandler(object):
    # constructor
    def __init__(self, filename, w_data, model):
        self.filename = filename                # a name to the file the user wants to create or read from
        self.w_data = w_data                    # data to be stored in pcap file
        self.r_data = None                      # data read from a pcap file
        self.model = model                      # data model used to display read data in table
        self.count = 0
        self.record = None

    ##################################################
    # METHODS
    ##################################################

    # a method to write in a pcapfile
    def write_pcap(self):
        # create a stream pcap writer
        pwriter = PcapWriter(self.filename, append=True)

        # write packets in pcap file
        for i in self.w_data:
            pwriter.write(i.pack)

        # close stream
        pwriter.close()

    # a method to read from a pcap file
    def read_pcap(self):
        # create a stream pcap reader
        preader = PcapReader(self.filename)

        # read all packets in file
        self.r_data = preader.read_all()

        # close stream
        preader.close()

    # a method that converts protocol numbers to their corresponding name
    def get_proto(self, protoNo):
        if (protoNo == 6):
            return "TCP"
        elif (protoNo == 17):
            return "UDP"
        elif (protoNo == 1):
            return "ICMP"

    # update model's underlaying data in a way to be updated all views
    def update_model(self):
        self.model.beginInsertRows(QtCore.QModelIndex(), self.count, self.count)
        self.w_data.append(self.record)
        self.count += 1
        self.model.endInsertRows()

    # a method to remove all rows in table
    def remove_all(self):
        self.model.beginRemoveRows(QtCore.QModelIndex(), 0, len(self.w_data))
        self.model.removeRows(0, len(self.w_data)-1, QtCore.QModelIndex())
        self.model.endRemoveRows()
        self.w_data.clear()

    # a method to classify read packets
    def classify(self, packets):
        # clear old data
        self.remove_all()

        for packet in packets:
            # create a record object
            self.record = Record(self.count, None)

            # store packet in record
            self.record.pack = packet

            # Ethernet (Create ethernet object that is stored in a record object)
            if (packet.haslayer('Ethernet')):
                eth_p = packet.getlayer('Ethernet')
                self.record.ethernet = Eth_part(eth_p.src, eth_p.dst, eth_p.type)

                self.record.information = f"Source mac: {eth_p.src} Destination mac: {eth_p.dst} Type: {eth_p.type}"

            # IP  (Create ip object that is stored in a record object)
            if (packet.haslayer('IP')):
                ip_p = packet.getlayer('IP')
                self.record.ip = IP_part(ip_p.version, ip_p.ihl, ip_p.tos, ip_p.len, ip_p.id, ip_p.flags, ip_p.frag, ip_p.ttl, self.get_proto(ip_p.proto), ip_p.chksum, ip_p.src, ip_p.dst)

            # ICMP  (Create ICMP object that is stored in a record object)
            if (packet.haslayer('ICMP')):
                icmp_p = packet.getlayer('ICMP')
                self.record.icmp = ICMP_part(icmp_p.type, icmp_p.code, icmp_p.chksum, icmp_p.id, icmp_p.seq)

                # setting info part
                self.record.information = f"Type: {icmp_p.type}, Code: {icmp_p.code}, Checksum:  {icmp_p.chksum}, Seq: {icmp_p.seq}"
            # TCP
            if (packet.haslayer('TCP')):
                tcp_p = packet.getlayer('TCP')
                self.record.tcp = TCP_part(tcp_p.sport, tcp_p.dport, tcp_p.seq, tcp_p.ack, tcp_p.dataofs, tcp_p.reserved, tcp_p.flags, tcp_p.window, tcp_p.chksum, tcp_p.urgptr, tcp_p.options)

                # setting info part
                self.record.information = f"Source Port: {tcp_p.sport} Destination Port: {tcp_p.dport}"
            # UDP
            if (packet.haslayer('UDP')):
                udp_p = packet.getlayer('UDP')
                self.record.udp = UDP_part(udp_p.sport, udp_p.dport, udp_p.len, udp_p.chksum)

                # setting info part
                self.record.information = f"Source Port: {udp_p.sport} Destination Port: {udp_p.dport} Length: {udp_p.len}"
            # Payload
            if (packet.haslayer('Raw')):
                self.record.load = Payload_part(packet.getlayer(Raw).load)

            self.update_model()
        self.count = 0