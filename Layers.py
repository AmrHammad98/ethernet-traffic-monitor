# Class describing ethernet part
class Eth_part(object):
    # constructor
    def __init__(self, src_mac, dst_mac, typ):
        self.src_mac = src_mac
        self.dst_mac = dst_mac
        self.typ = typ

# Class describing the ICMP part
class ICMP_part(object):
    # constructor
    def __init__(self, typ, code, checksum, iden, seq):
        self.typ = typ
        self.code = code
        self.checksum = checksum
        self.iden = iden
        self.seq = seq

# Class that describes the ip part
class IP_part(object):
    # constructor
    def __init__(self, ver, ihl, tos, length, iden, flags, frag, ttl, prtcl, checksum, src_ip, dst_ip):
        self.ver = ver
        self.ihl = ihl
        self.tos = tos
        self.length = length
        self.iden = iden
        self.flags = flags
        self.frag = frag
        self.ttl = ttl
        self.prtcl = prtcl
        self.checksum = checksum
        self.src_ip = src_ip
        self.dst_ip = dst_ip

# Class describing TCP part
class TCP_part(object):
    # constructor
    def __init__(self, src_port, dst_port, seq, ack, offset, reserved, flags, window, checksum, urgptr, options):
        self.src_port = src_port
        self.dst_port = dst_port
        self.seq = seq
        self.ack = ack
        self.offset = offset
        self.reserved = reserved
        self.flags = flags
        self.window = window
        self.checksum = checksum
        self.urgptr = urgptr
        self.options = options

# Class describing UDP part
class UDP_part(object):
    # constructor
    def __init__(self, src_port, dst_port, length, checksum):
        self.src_port = src_port
        self.dst_port = dst_port
        self.length = length
        self.checksum = checksum

# Class describing payload part
class Payload_part(object):
    def __init__(self, load):
        self.load = load