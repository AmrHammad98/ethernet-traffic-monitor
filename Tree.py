from PyQt5.QtWidgets import QTreeWidgetItem

# a class that handles Tree view in the main window
class Tree(object):
    # constructor
    def __init__(self, widget):
        self.widget = widget                # tree widget that is being used

        # roots
        self.ether_root = None
        self.ip_root = None
        self.trans_layer_root = None

        # Ethernet children
        self.destination = None
        self.source = None
        self.typ = None
        self.eth_load = None

        # IP children
        self.version = None
        self.headlen = None
        self.typserv = None
        self.length_ip = None
        self.ID = None
        self.flagz = None
        self.fra = None
        self.timetiliv = None
        self.protocol = None
        self.chcksum = None
        self.srcip = None
        self.dstip = None
        self.ip_load = None

        # TCP
        self.spo_tcp = None
        self.dpo_tcp = None
        self.seq_tcp = None
        self.ackno = None
        self.dofsset = None
        self.res = None
        self.flags_tcp = None
        self.wind = None
        self.checksum_tcp = None
        self.urg = None
        self.op = None
        self.tcp_load = None

        # UDP
        self.spo = None
        self.dpo = None
        self.length_udp = None
        self.checksum_udp = None
        self.udp_load = None

        #ICMP
        self.type_icmp = None
        self.code = None
        self.checksum_icmp = None
        self.ID_icmp = None
        self.seq = None

        # add roots to widget
        self.addRoots()

        # flags to flag children added
        self.ip_flag = False
        self.tcp_flag = False
        self.udp_flag = False
        self.icmp_flag = False
        self.ether_flag = False

    # method to add tree roots to widget
    def addRoots(self):
        # create widget items for roots
        self.ether_root = QTreeWidgetItem(self.widget)
        self.ip_root = QTreeWidgetItem(self.widget)
        self.trans_layer_root = QTreeWidgetItem(self.widget)

        # set root's text
        self.ether_root.setText(0, "Ethernet")
        self.ip_root.setText(0, "Internet Protocol")
        self.trans_layer_root.setText(0, "Transport Layer Protocol")

        # set items to be top level items
        self.widget.addTopLevelItem(self.ether_root)
        self.widget.addTopLevelItem(self.ip_root)
        self.widget.addTopLevelItem(self.trans_layer_root)

    # a method to add and update children to specified root
    def addChildren(self, part):
        # create widget items and make ether as parent
        if part == 0 and self.ether_flag == False:
            self.destination = QTreeWidgetItem(self.ether_root, 1)
            self.source = QTreeWidgetItem(self.ether_root, 2)
            self.typ = QTreeWidgetItem(self.ether_root, 3)
            self.eth_load = QTreeWidgetItem(self.ether_root, 29)

            self.ether_flag = True

        # create widget items and make ip as parent
        elif part == 1 and self.ip_flag == False:
            self.version = QTreeWidgetItem(self.ip_root, 4)
            self.headlen = QTreeWidgetItem(self.ip_root, 5)
            self.typserv = QTreeWidgetItem(self.ip_root, 6)
            self.length_ip = QTreeWidgetItem(self.ip_root, 7)
            self.ID = QTreeWidgetItem(self.ip_root, 8)
            self.flagz = QTreeWidgetItem(self.ip_root, 9)
            self.fra = QTreeWidgetItem(self.ip_root, 10)
            self.timetiliv = QTreeWidgetItem(self.ip_root, 11)
            self.protocol = QTreeWidgetItem(self.ip_root, 12)
            self.chcksum = QTreeWidgetItem(self.ip_root, 13)
            self.srcip = QTreeWidgetItem(self.ip_root, 14)
            self.dstip = QTreeWidgetItem(self.ip_root, 15)
            self.ip_load = QTreeWidgetItem(self.ip_root, 30)

            self.ip_flag = True

        # TCP
        # create widget items and set transport layer as parent
        elif part == 2 and self.tcp_flag == False:
            self.spo_tcp = QTreeWidgetItem(self.trans_layer_root, 20)
            self.dpo_tcp = QTreeWidgetItem(self.trans_layer_root, 21)
            self.seq_tcp = QTreeWidgetItem(self.trans_layer_root, 22)
            self.ackno = QTreeWidgetItem(self.trans_layer_root, 23)
            self.dofsset = QTreeWidgetItem(self.trans_layer_root)
            self.res = QTreeWidgetItem(self.trans_layer_root)
            self.flags_tcp = QTreeWidgetItem(self.trans_layer_root, 24)
            self.wind = QTreeWidgetItem(self.trans_layer_root, 25)
            self.checksum_tcp = QTreeWidgetItem(self.trans_layer_root, 26)
            self.urg = QTreeWidgetItem(self.trans_layer_root, 27)
            self.op = QTreeWidgetItem(self.trans_layer_root, 28)
            self.tcp_load = QTreeWidgetItem(self.trans_layer_root, 31)

            self.tcp_flag = True

        # UDP
        # create widget items and set transport layer as parent
        elif part == 3 and self.udp_flag == False:
            self.spo = QTreeWidgetItem(self.trans_layer_root, 16)
            self.dpo = QTreeWidgetItem(self.trans_layer_root, 17)
            self.length_udp = QTreeWidgetItem(self.trans_layer_root, 18)
            self.checksum_udp = QTreeWidgetItem(self.trans_layer_root, 19)
            self.udp_load = QTreeWidgetItem(self.trans_layer_root, 32)

            self.udp_flag = True

        # ICMP
        # create widget items and set transport layer as parent
        elif part == 4 and self.icmp_flag == False:
            self.type_icmp = QTreeWidgetItem(self.trans_layer_root)
            self.code = QTreeWidgetItem(self.trans_layer_root)
            self.checksum_icmp = QTreeWidgetItem(self.trans_layer_root)
            self.ID_icmp = QTreeWidgetItem(self.trans_layer_root)
            self.seq = QTreeWidgetItem(self.trans_layer_root)

            self.icmp_flag = True

    # a method to remove children according to specified part
    def removeChild(self, part):
        # Ethernet = 0
        if part == 0 and self.ether_flag == True:
            self.ether_root.removeChild(self.destination)
            self.ether_root.removeChild(self.source)
            self.ether_root.removeChild(self.typ)
            self.ether_root.removeChild(self.eth_load)

            self.ether_flag = False

        # ip = 1
        elif part == 1 and self.ip_flag == True:
            self.ip_root.removeChild(self.version)
            self.ip_root.removeChild(self.headlen)
            self.ip_root.removeChild(self.typserv)
            self.ip_root.removeChild(self.length_ip)
            self.ip_root.removeChild(self.ID)
            self.ip_root.removeChild(self.flagz)
            self.ip_root.removeChild(self.fra)
            self.ip_root.removeChild(self.timetiliv)
            self.ip_root.removeChild(self.protocol)
            self.ip_root.removeChild(self.chcksum)
            self.ip_root.removeChild(self.srcip)
            self.ip_root.removeChild(self.dstip)
            self.ip_root.removeChild(self.ip_load)

            self.ip_flag = False

        # TCP = 2
        elif part == 2 and self.tcp_flag == True:
            self.trans_layer_root.removeChild(self.spo_tcp)
            self.trans_layer_root.removeChild(self.dpo_tcp)
            self.trans_layer_root.removeChild(self.seq_tcp)
            self.trans_layer_root.removeChild(self.ackno)
            self.trans_layer_root.removeChild(self.dofsset)
            self.trans_layer_root.removeChild(self.res)
            self.trans_layer_root.removeChild(self.flags_tcp)
            self.trans_layer_root.removeChild(self.wind)
            self.trans_layer_root.removeChild(self.checksum_tcp)
            self.trans_layer_root.removeChild(self.urg)
            self.trans_layer_root.removeChild(self.op)
            self.trans_layer_root.removeChild(self.tcp_load)

            self.tcp_flag = False

        # UDP = 3
        elif part == 3 and self.udp_flag == True:
            self.trans_layer_root.removeChild(self.spo)
            self.trans_layer_root.removeChild(self.dpo)
            self.trans_layer_root.removeChild(self.length_udp)
            self.trans_layer_root.removeChild(self.checksum_udp)
            self.trans_layer_root.removeChild(self.udp_load)

            self.udp_flag = False

        # ICMP = 4
        elif part == 4 and self.icmp_flag == True:
            self.trans_layer_root.removeChild(self.type_icmp)
            self.trans_layer_root.removeChild(self.code)
            self.trans_layer_root.removeChild(self.checksum_icmp)
            self.trans_layer_root.removeChild(self.ID_icmp)
            self.trans_layer_root.removeChild(self.seq)

            self.icmp_flag = False

    # a method that updates children by removal or addition
    def updateChild(self, record):
        # Ethernet
        if record.ethernet is not None:
            # add children to ethernet root
            self.addChildren(0)

            # set text for Ethernet children
            self.destination.setText(0, f"Destination Mac Address: {record.ethernet.dst_mac}")
            self.source.setText(0, f"Source Mac Address: {record.ethernet.src_mac}")
            self.typ.setText(0, f"Type: {record.ethernet.typ}")
            self.eth_load.setText(0, "Select for Ethernet Payload")
        else:
            self.removeChild(0)

        # Ip
        if record.ip is not None:
            # add children to ip root
            self.addChildren(1)

            # set text for children
            self.version.setText(0, f"Version: {record.ip.ver}")
            self.headlen.setText(0, f"Header Length: {record.ip.ihl}")
            self.typserv.setText(0, f"Type of Service: {record.ip.tos}")
            self.length_ip.setText(0, f"Length: {record.ip.length}")
            self.ID.setText(0, f"ID: {record.ip.iden}")
            self.flagz.setText(0, f"Flags: {record.ip.flags}")
            self.fra.setText(0, f"Frag: {record.ip.frag}")
            self.timetiliv.setText(0, f"Time to live: {record.ip.ttl}")
            self.protocol.setText(0, f"Protocol: {record.ip.prtcl}")
            self.chcksum.setText(0, f"Checksum: {record.ip.checksum}")
            self.srcip.setText(0, f"Source IP: {record.ip.src_ip}")
            self.dstip.setText(0, f"Destination IP: {record.ip.dst_ip}")
            self.ip_load.setText(0, "Select for IP Payload")
        else:
            self.removeChild(1)

        if record.tcp is not None:
            # add tcp children to transport layer root
            self.addChildren(2)

            # remove other parts if found
            self.removeChild(3)
            self.removeChild(4)

            # change text to desired protocol
            self.trans_layer_root.setText(0, "Transmission Control Protocol")

            self.spo_tcp.setText(0, f"Source Port: {record.tcp.src_port}")
            self.dpo_tcp.setText(0, f"Destination Port: {record.tcp.dst_port}")
            self.seq_tcp.setText(0, f"Sequence: {record.tcp.seq}")
            self.ackno.setText(0, f"Acknowledgment: {record.tcp.ack} ")
            self.dofsset.setText(0, f"Data Offset: {record.tcp.offset}")
            self.res.setText(0, f"Reserved: {record.tcp.reserved}")
            self.flags_tcp.setText(0, f"Flags: {record.tcp.flags}")
            self.wind.setText(0, f"Window: {record.tcp.window}")
            self.checksum_tcp.setText(0, f"Checksum: {record.tcp.checksum}")
            self.urg.setText(0, f"Urgent Pointer: {record.tcp.urgptr}")
            self.op.setText(0, f"Options: {record.tcp.options}")
            self.tcp_load.setText(0, "Select for TCP Payload")

        if record.udp is not None:
            # add udp children to transport layer root
            self.addChildren(3)

            # remove other parts if found
            self.removeChild(2)
            self.removeChild(4)

            # change text to desired protocol
            self.trans_layer_root.setText(0, "User Datagram Protocol")
            # set text for created items
            if record.udp is not None:
                self.spo.setText(0, f"Source Port: {record.udp.src_port}")
                self.dpo.setText(0, f"Destination Port: {record.udp.dst_port}")
                self.length_udp.setText(0, f"Length: {record.udp.length}")
                self.checksum_udp.setText(0, f"Checksum: {record.udp.checksum}")
                self.udp_load.setText(0, "Select for UDP Payload")

        if record.icmp is not None:
            # add icmp children to transport layer root
            self.addChildren(4)

            # remove other parts if found
            self.removeChild(2)
            self.removeChild(3)

            # change text to desired protocol
            self.trans_layer_root.setText(0, "Internet Control Message Protocol")
            # set text for created items
            if record.icmp is not None:
                self.type_icmp.setText(0, f"Type: {record.icmp.typ}")
                self.code.setText(0, f"Code: {record.icmp.code}")
                self.checksum_icmp.setText(0, f"Checksum: {record.icmp.checksum}")
                self.ID_icmp.setText(0, f"ID: {record.icmp.iden}")
                self.seq.setText(0, f"Sequence Number: {record.icmp.seq}")