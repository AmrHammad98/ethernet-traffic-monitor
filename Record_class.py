# A class Record that holds all info about a network
class Record(object):
    # constructor
    def __init__(self, id, time):
        self.id = id                                             # id of record
        self.time = time                                         # time where the communication started
        self.ethernet = None                                     # Ethernet part
        self.ip = None                                           # Ip part
        self.icmp = None                                         # ICMP part
        self.tcp = None                                          # TCP part
        self.udp = None                                          # UDP part
        self.load = None                                         # payload
        self.information = None                                  # info to display
        self.pack = None                                       # packet to be classified (stored to use hexdump)