from PyQt5 import QtCore

# table model class subclassed from QAbstracttablemodel class
class TableModel(QtCore.QAbstractTableModel):

    # constructor
    def __init__(self, data=[], headers=[], parent=None):
        # call parent class constructor
        QtCore.QAbstractTableModel.__init__(self, parent)
        self.__data = data                                           # private 1D array that holds Record objects
        self.__headers = headers                                     # private 1D array that holds table headers

    ##################################################
    # METHODS TO OVERRIDE
    ##################################################

    # rowCount method to return number of rows in table
    def rowCount(self, parent=None, *args, **kwargs):
        return len(self.__data)

    # columnCount method to return number of columns in table
    def columnCount(self, parent=None, *args, **kwargs):
        return 6

    # flag method for table items
    def flags(self, QModelIndex):
        # make items enabled editable and selectable`
        return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    # data method to specify where does the data come from
    def data(self, QModelIndex, role=None):
        # display role
        if role == QtCore.Qt.DisplayRole:
            row = QModelIndex.row()
            column = QModelIndex.column()
            if column == 0:
                if self.__data[row].id != None:
                    return self.__data[row].id
                else:
                    return""
            elif column == 1:
                if self.__data[row].time:
                    return self.__data[row].time
                else:
                    return""
            elif column == 2:
                if self.__data[row].ip != None:
                    return self.__data[row].ip.src_ip
                else:
                    return"None"
            elif column == 3:
                if self.__data[row].ip != None:
                    return self.__data[row].ip.dst_ip
                else:
                    return"None"
            elif column == 4:
                if self.__data[row].ip != None:
                    return self.__data[row].ip.prtcl
                else:
                    return"Ethernet"
            elif column == 5:
                if self.__data[row].information != None:
                    return self.__data[row].information
                else:
                    return"None"

    # header data method to label the headers
    def headerData(self, p_int, Qt_Orientation, role=None):
        if role == QtCore.Qt.DisplayRole:
            if Qt_Orientation == QtCore.Qt.Horizontal:
                return self.__headers[p_int]
            elif Qt_Orientation == QtCore.Qt.Vertical:
                pass